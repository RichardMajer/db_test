<?php


class Dbconnect
{
    private $serverName;
    private $userName;
    private $password;
    private $dbname;
    private $charset;

    /**
     *
     */
    public function connect() {
        $this->serverName = 'localhost';
        $this->userName = 'root';
        $this->password = 'root';
        $this->dbname = 'movies';
        $this->charset = 'utf8';


        $dsn = "mysql:host=".$this->serverName.";dbane=".$this->dbname.";charset=".$this->charset;
        $db = new PDO($dsn, $this->userName, $this->password);

        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);


        try {
            $sql = "SELECT * from movies.movies WHERE id > 4 ORDER by year";
            $result = $db->query($sql);
            $result = $result->fetchAll( PDO::FETCH_ASSOC);

                echo '<ul class="list-unstyled">';
            foreach ($result as $column): ?>
                <li>title:  <?= $column['title'] ?> </li>
                <li>id:  <?= $column['id'] ?> </li>
                <li>year:  <?= $column['year'] ?> </li>
                <hr>
            <? endforeach;
                echo '</ul>';

            echo "<pre>";
            print_r($result );
            echo "</pre>";

        } catch (PDOException $e) {

            echo "<pre>";
            print_r($e->getMessage() );
            echo "</pre>";

            $errMessage = date('j M Y, G:i').PHP_EOL.$e->getMessage().PHP_EOL;
            
            file_put_contents('error.txt', $errMessage, FILE_APPEND);

        }
    }
}